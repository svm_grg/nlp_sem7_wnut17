import torch
from torch import autograd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import time
import os
import numpy as np

import data_loader
import architecture
import utils 

# whether to use GPU 
cuda = False

# create the network
input_size = 91
output_size = 13
hidden_size = 32
hidden_size2 = 64
num_layers = 1
bidirectional = True
dropout_prob = 0.9
model = architecture.net(input_size, output_size, hidden_size,
                         hidden_size2, num_layers, bidirectional, dropout_prob)
if cuda:
    model.cuda()

# load data
batch_size = 1

root = 'dataset'
train_file = 'wnut17train.conll'
val_file = 'emerging.dev.conll'

_, _, DICT = data_loader.data_wnut.load(os.path.join(root, train_file))

train_set = data_loader.data_wnut(root, train_file, DICT)
train_loader = torch.utils.data.DataLoader(train_set, batch_size=batch_size,
                                           shuffle=True, num_workers=1)

val_set = data_loader.data_wnut(root, val_file, DICT)
val_loader = torch.utils.data.DataLoader(val_set, batch_size=batch_size,
                                           shuffle=True, num_workers=1)


# Loss criterion
# Count of instances for different classes (Training)
# [50407, 660, 326, 548, 238, 221, 44, 142, 192, 140, 202, 264, 147]

weight_tensor = torch.Tensor([2.72448418e-04, 2.08080415e-02, 4.21267099e-02,
                              2.50607800e-02, 5.77029723e-02, 6.21416625e-02,
                              3.12120623e-01, 9.67134325e-02, 7.15276428e-02,
                              9.80950530e-02, 6.79866704e-02, 5.20201038e-02,
                              9.34238600e-02])
if cuda:
    criterion = nn.CrossEntropyLoss(weight_tensor).cuda()
else:
    criterion = nn.CrossEntropyLoss(weight_tensor)

     
# Optimizer
lr = 1e-3
weight_decay = 1e-5
optim_name = 'adam'
optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=weight_decay)

# train the network
begin_time = time.time()
no_epochs = 1
storage = utils.store_data(lr, weight_decay, no_epochs)

print('lr:{} wd:{} dp:{} h1:{} h2:{}'.format(lr, weight_decay, dropout_prob,
                                            hidden_size, hidden_size2))

try:
    for epoch in range(no_epochs):

        # training
        predicted_T = []
        target_T = []    
        LOSS_T = 0
        model.train()
        for i, (input, target) in enumerate(train_loader):
            if cuda:
                target = target.cuda()
                input = input.cuda()

            # wrap the tensors into variables
            target_var = torch.autograd.Variable(target)
            input_var = torch.autograd.Variable(input[0]) 

            # compute output
            output = model(input_var, cuda)
            loss = criterion(output, target_var[0])
            LOSS_T += loss.data[0]

            # compute gradient and do SGD step
            model.zero_grad()
            loss.backward()
            optimizer.step()

            if i % 1000 == 0:
                print(i, loss.data[0])

            predicted_T += list(torch.max(output, 1)[1].data)
            target_T += list(target_var[0].data)

        LOSS_T /= (i+1)


        # validation
        predicted_V = []
        target_V = []
        LOSS_V = 0
        model.eval()
        for i, (input, target) in enumerate(val_loader):
            if cuda:
                target = target.cuda()
                input = input.cuda()

            # wrap the tensors into variables
            target_var = torch.autograd.Variable(target)
            input_var = torch.autograd.Variable(input[0]) 

            # compute output
            output = model(input_var, cuda)
            loss = criterion(output, target_var[0])
            LOSS_V += loss.data[0]

            predicted_V += list(torch.max(output, 1)[1].data)
            target_V += list(target_var[0].data)

        LOSS_V /= (i+1)

        print('Epoch:[{}] \t LOSS_T:{} \t LOSS_V:{}\t Time Taken:{:.1f}'.format(epoch, LOSS_T, LOSS_V,
                                                                                time.time()-begin_time))

        # print data at the end of epoch
        l_T = utils.prec_rec(predicted_T, target_T)
        l_V = utils.prec_rec(predicted_V, target_V)

        print('\t \t Class \t Pr_T \t Rc_T \t F1_T \t'
              'Pr_V \t Rc_V \t F1_V')

        for i in range(len(l_T)):
            (precision_T, recall_T, F1_T) = l_T[i]
            (precision_V, recall_V, F1_V) = l_V[i]

            print('\t \t {} \t {:.3f} \t {:.3f} \t {:.3f} \t'
                  '{:.3f} \t {:.3f} \t {:.3f}'.format(i, precision_T, recall_T, F1_T,
                                                       precision_V, recall_V, F1_V))


        storage.update(model, optimizer,
                       LOSS_T, np.mean(l_T[:, 2]),
                       LOSS_V, np.mean(l_V[:, 2]))


    storage.update_time(time.time()-begin_time)
    storage.dump()
except KeyboardInterrupt:
    print('-' * 70)
    print('Interrupted by user, exiting from training early')
    storage.update_time(time.time()-begin_time)
    storage.dump()
