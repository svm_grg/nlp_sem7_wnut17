# README # 

This code requires PyTorch (http://pytorch.org/).

Contents
1. main.py
2. architecture.py : Definition for the neural network architecture
3. data\_loader.py  : Data loader (requires the data to be present in a folder named as 'dataset/')
4. utils.py        : for storing intermediate neural net states and other utilities