import json
import pandas as pd
import time
import matplotlib.pyplot as plt
import os
import numpy as np
from sklearn.metrics import precision_recall_fscore_support as prfs

import torch

def prec_rec(predicted, target):
    return np.column_stack(prfs(predicted, target)[0:3])

class store_data(object):
    def __init__(self, lr, weight_decay, no_epochs):

        self.lr = lr
        self.weight_decay = weight_decay
        self.no_epochs = no_epochs
        self.ctime = time.strftime("%Y%m%d_%H%M%S", time.gmtime())
        self.file_name = 'MODEL__'+self.ctime
        self.max_prec = 0
        self.LOSS_T = []
        self.PREC_T = []
        self.LOSS_V = []
        self.PREC_V = []
        self.count = 0
        self.dir = self.file_name+'.data'
        os.makedirs(self.dir)

    def update_time(self, time_taken):
        self.time_taken = time_taken

    def update(self, model, optimizer, loss_t, prec_t, loss_v, prec_v):
        self.LOSS_T.append(loss_t)
        self.PREC_T.append(prec_t)
        self.LOSS_V.append(loss_v)        
        self.PREC_V.append(prec_v)
        
        if prec_v > self.max_prec:
            with open(os.path.join(self.dir, self.file_name+'.pt'), 'wb') as f:
                dict_ = {
                    'model': model.state_dict(),
                    'optimizer': optimizer.state_dict()
                }
                torch.save(dict_, f)
            self.max_prec = prec_v
            self.prec_train_at_ = prec_t
            self.max_epoch = self.count
            
        self.count += 1
            
    def dump(self):
        dat = {'lr': self.lr,
               'weight_decay': self.weight_decay,
               'epoch': self.no_epochs,
               'time_taken': self.time_taken,
               'max_prec': self.max_prec,
               'max_epoch': self.max_epoch,
               'prec_train': self.prec_train_at_}
        with open(os.path.join(self.dir, self.file_name+'.json'), 'w') as f:
            json.dump(dat, f)
        
        pd.DataFrame({'loss_t': self.LOSS_T,
                      'prec_t': self.PREC_T,
                      'loss_v': self.LOSS_V,
                      'prec_v': self.PREC_V}).to_csv(os.path.join(self.dir, self.file_name+'.csv'),
                                                index=False, sep=',')

        print('Training:{} \t Validation:{}'.format(self.prec_train_at_, self.max_prec))
        
    @staticmethod
    def plot_data(file_name):
        dat = pd.read_csv(os.path.join(file_name+'.data', file_name+'.csv'))

        with open(os.path.join(file_name+'.data', file_name+'.json'), 'r') as f:
            dat_json = json.loads(next(f))

        plot_title = 'Adam({}) \n [Time Taken:{time_taken:.0f}]'.format(
            dat_json['lr'], time_taken=dat_json['time_taken'])
        
        plt.figure()
        plt.suptitle(plot_title)
        
        plt.subplot(211)
        plt.plot(dat['loss_t'], label='training')
        plt.plot(dat['loss_v'], label='validation')
        plt.ylabel('Loss')
        plt.legend()

        plt.subplot(212)
        plt.plot(dat['prec_t'], label='training')
        plt.plot(dat['prec_v'], label='validation')
        plt.xlabel('Epochs')
        plt.ylabel('AVG F1 score')
        plt.legend()
        
        plt.savefig(os.path.join(file_name+'.data', file_name+'.png'))
        plt.show()
