import numpy as np
import torch
import torch.utils.data as data

import os
import string

import pdb

class data_wnut(data.Dataset):

    def __init__(self, root, file_name, DICT):
        data_set = os.path.join(root, file_name)
        self.SENT, self.TAGS, _ = self.load(data_set)
        self.DICT = DICT
        self.num_of_char = len(self.DICT)+1 # extra 1 for <UNK>

        
    def __getitem__(self, index):
        seq = self.SENT[index]
        sentence = []
        max_word_length = -1
        for word in seq:
            vec = np.zeros((0,self.num_of_char))
            for ch in word:
                y = np.zeros((1,self.num_of_char))
                y[0,self.DICT.get(ch, self.num_of_char-1)] = 1
                vec = np.concatenate((vec,y),0)
            sentence.append(vec)
            if vec.shape[0] > max_word_length:
                max_word_length = vec.shape[0]
                
        for i in range(len(sentence)):
            word = sentence[i]
            Z = np.zeros((max_word_length - word.shape[0], word.shape[1]))
            
            word = torch.Tensor(np.concatenate((word, Z), 0))
            sentence[i] = torch.Tensor(word).view(1, word.shape[0], word.shape[1])

        sentence = torch.cat(sentence, 0)

        tag = torch.Tensor(self.TAGS[index]).long()

        return sentence, tag


    @staticmethod
    def load(data_set):
        f = open(data_set, 'r')
        tag_list = ['O', 'B-person', 'I-person', 'B-location', 'I-location', 'B-corporation',
                    'I-corporation', 'B-product', 'I-product', 'B-creative-work',
                    'I-creative-work', 'B-group', 'I-group']
        Sentences = []
        Tags = []
        tmp1 = []
        tmp2 = []
        for x in f:
            if x != '\n' and x != '\t\n' and x[0] != '\ufeff':
                x = x[0:-1] # ignoring the '\n' at the end

                x = x.split('\t')

                if (x[0][0:7] == 'http://' or
                    x[0].translate(str.maketrans('','',string.punctuation)) == ''):
                    continue

                tmp1.append(x[0])

                tag = tag_list.index(str(x[1]))
                tmp2.append(tag)

            else:
                if tmp1 != []:
                    Sentences.append(tmp1)
                    Tags.append(tmp2)

                tmp1 = []
                tmp2 = []


        # creating a dictionary
        l = []
        for x in Sentences:
            l += x
        dictionary = set()
        for word in l:
            for char in word:
                dictionary.add(char)
        dictionary = list(dictionary)
        dict_look = {}
        for i in range(len(dictionary)):
            dict_look[dictionary[i]] = i

        return Sentences, Tags, dict_look
    
    def __len__(self):
        return len(self.SENT)

