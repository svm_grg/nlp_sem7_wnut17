import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import autograd

class net(nn.Module):
    def __init__(self, input_size=91, output_size=13, hidden_size=30, hidden_size2=60,
                 num_layers=1, bidirectional=False, dropout_prob=0):
        super(net, self).__init__()

        self.input_size = input_size
        self.output_size = output_size
        self.hidden_size = hidden_size
        self.hidden_size2 = hidden_size2
        self.num_layers = num_layers
        if bidirectional:
            self.num_directions = 2
        else:
            self.num_directions = 1

        self.lstm_c = nn.LSTM(input_size, hidden_size, num_layers=num_layers, batch_first=True)
        self.lstm_w = nn.LSTM(hidden_size, hidden_size2, num_layers=num_layers,
                              batch_first=True, bidirectional=bidirectional)

        self.drop = nn.Dropout(p=dropout_prob)
        self.mlp = nn.Linear(hidden_size2*self.num_directions, output_size)
        
    def forward(self, input, cuda=False):

        hidden_c, hidden_w = self.init_hidden(input.size(0), 1, cuda)

        feat, _ = self.lstm_c(input, hidden_c)
        feat = feat[:,-1,:].unsqueeze(0)

        output, _ = self.lstm_w(feat, hidden_w)

        output = self.drop(output)
        output = self.mlp(output)

        return output[0]

    def init_hidden(self, batch_size_c, batch_size_w, cuda=False):
            
        if cuda:
            hidden_c = (
                autograd.Variable(torch.zeros(self.num_layers*1, batch_size_c,
                                              self.hidden_size).cuda()),
                autograd.Variable(torch.zeros(self.num_layers*1, batch_size_c,
                                              self.hidden_size).cuda()))
            hidden_w = (
                autograd.Variable(torch.zeros(self.num_layers*self.num_directions, batch_size_w,
                                              self.hidden_size2).cuda()),
                autograd.Variable(torch.zeros(self.num_layers*self.num_directions, batch_size_w,
                                              self.hidden_size2).cuda()))
        else:
            hidden_c = (autograd.Variable(torch.zeros(self.num_layers*1, batch_size_c,
                                                      self.hidden_size)),
                        autograd.Variable(torch.zeros(self.num_layers*1, batch_size_c,
                                                      self.hidden_size)))
            hidden_w = (autograd.Variable(torch.zeros(self.num_layers*self.num_directions, batch_size_w,
                                                      self.hidden_size2)),
                        autograd.Variable(torch.zeros(self.num_layers*self.num_directions, batch_size_w,
                                                      self.hidden_size2)))

        return hidden_c, hidden_w
